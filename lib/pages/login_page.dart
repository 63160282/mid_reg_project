import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/home_page.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Spacer(flex: 5),
            Image.network(
              'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/602px-Buu-logo11.png?20221010124315'
              ),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            //const SizedBox(height: 30.0),
            const Spacer(flex: 3),
            ElevatedButton(onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => RegisterPage1()),
              );
            },
                child: const Text('Login')
            ),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
