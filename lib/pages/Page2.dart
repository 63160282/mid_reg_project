import 'package:flutter/material.dart';

class TimetablePage2 extends StatelessWidget {
  final List<String> _classNames = ["Web Programming","Object-Oriented Analysis and Design","Software Testing","Multimedia Programming for Multiplatforms","Mobile Application Development I","Introduction to Natural Language Processing",  ];

  final List<String> _classTimes = ["08:00-09:00","09:00-10:00","10:00-11:00","11:00-12:00","13:00-14:00","14:00-15:00","15:00-16:00"  ];

  final List<String> _examNames = ["Web Programming","Object-Oriented Analysis and Design","Software Testing","Multimedia Programming for Multiplatforms","Mobile Application Development I","Introduction to Natural Language Processing",  ];

  final List<String> _examDates = ["10/5/2023","12/5/2023","14/5/2023","16/5/2023", "18/5/2023","20/5/2023","22/5/2023"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ตารางเรียน/สอบ"),
        centerTitle: true,
        backgroundColor: Color.fromARGB(255, 255, 211, 14),
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 8,
          ),
          Text(
            'มหาวิทยาลัยบูรพา',
            style: TextStyle(
              color: Color.fromARGB(255, 253, 234, 64),
              fontSize: 32,
              fontWeight: FontWeight.bold,
              letterSpacing: 1.5,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            'ระบบ REG มหาวิทยาลัยบูรพา',
            style: TextStyle(
              color: Colors.amber[600],
              fontSize: 22,
              letterSpacing: 1.2,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 16,
          ),
          Expanded(
            child: Table(
              border: TableBorder.all(
                color: Color.fromARGB(255, 109, 107, 107),
                width: 1,
              ),
              columnWidths: {
                0: FixedColumnWidth(100.0),
                1: FixedColumnWidth(100.0),
                2: FixedColumnWidth(100.0),
                3: FixedColumnWidth(100.0),
                4: FixedColumnWidth(100.0),
                5: FixedColumnWidth(100.0),
              },
              
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Text(
                        "Time",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    for (String className in _classNames)
                      TableCell(
                        child: Text(
                          className,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      )
                  ],
                ),
                for (String classTime in _classTimes)
                  TableRow(
                    children: [
                      TableCell(
                        child: Text(classTime),
                      ),
                      for (String className in _classNames)
                        TableCell(
                          child: Text(""),
                        )
                    ],
                  )
              ],
            ),
          ),
          Expanded(
            child: Table(
              border: TableBorder.all(),
              columnWidths: {
                0: FixedColumnWidth(100.0),
                1: FixedColumnWidth(100.0),
              },
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Text(
                        "Exam Name",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    TableCell(
                      child: Text(
                        "Exam Date",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                for (int i = 0; i < _examNames.length; i++)
                  TableRow(
                    children: [
                      TableCell(
                        child: Text(_examNames[i]),
                      ),
                      TableCell(
                        child: Text(_examDates[i]),
                      ),
                    ],
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
