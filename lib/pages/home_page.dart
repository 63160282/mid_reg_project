import 'package:flutter/material.dart';
import 'package:flutter_application_1/utill/emoticon.dart';

import 'Page2.dart';
import 'Page3.dart';
import 'Page4.dart';


class RegisterPage1 extends StatelessWidget {
  const RegisterPage1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 154, 157, 160),
      body: SafeArea(
        child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //Hi
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'มหาวิทยาลัยบูรพา',
                          style: TextStyle(
                            color: Color.fromARGB(255, 253, 234, 64),
                            fontSize: 27,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'ระบบ REG มหาวิทยาลัยบูรพา',
                          style: TextStyle(
                            color: Colors.amber[600],
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),

                    //Not
                    Container(
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 153, 169, 182),
                          borderRadius: BorderRadius.circular(12)),
                      padding: EdgeInsets.all(12),
                      child: Icon(
                        Icons.language,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                //search bar
                Container(
                  decoration: BoxDecoration(
                      color: Colors.amber[50],
                      borderRadius: BorderRadius.circular(12)),
                  padding: EdgeInsets.all(12),
                  child: Row(
                    children: [
                      Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Search',
                        style: TextStyle(
                          color: Color.fromARGB(255, 160, 143, 143),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 25,
                ),

                Row(
                  children: [
                    Text(
                      '63160282 : นางสาวจิรารัตน์ ภู่ภมร',
                      style: TextStyle(
                        color: Color.fromARGB(255, 11, 15, 73),
                        fontSize: 22,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 20),

                Row(
                  children: [
                    Icon(
                      Icons.event,
                      size: 50,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                    SizedBox(height: 20),
                    Text(
                      'ตารางเรียน/สอบ',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                    ),
                    SizedBox(height: 5),
                    const Spacer(flex: 10),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TimetablePage2()),
                        );
                      },
                      child: Text('ต่อไป'),
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 238, 6),
                        padding:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                        textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),

                Row(
                  children: [
                    Icon(
                      Icons.perm_contact_cal,
                      size: 50,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                    SizedBox(height: 20),
                    Text(
                      'ประวัตินิสิต',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                    ),
                    SizedBox(height: 5),
                    const Spacer(flex: 10),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StudentPage3()),
                        );
                      },
                      child: Text('ต่อไป'),
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 238, 6),
                        padding:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                        textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 20),

                Row(
                  children: [
                    Icon(
                      Icons.person,
                      size: 50,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                    SizedBox(height: 20),
                    Text(
                      'ลงทะเบียน',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.normal,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                    ),
                    SizedBox(height: 5),
                    const Spacer(flex: 10),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegisterPage()),
                        );
                      },
                      child: Text('ต่อไป'),
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 255, 238, 6),
                        padding:
                            EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                        textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
