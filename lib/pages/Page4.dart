import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ลงทะเบียน'),
        ),
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                '63160282 : นางสาวจิรารัตน์ ภู่ภมร : คณะวิทยาการสารสนเทศ',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text(
                'หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -',
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 10),
              Text(
                'สถานภาพ: กำลังศึกษา',
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 10),
              Text(
                'อ. ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 20),
              Text(
                'รายวิชาที่ต้องการลงทะเบียน',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text(
                'เมื่อเลือกรายวิชาครบแล้ว คลิกปุ่มเข้าสู่หน้าจอยืนยันลงทะเบียน',
                style: TextStyle(fontSize: 16),
              ),
              SizedBox(height: 20),
              Text(
                'ภาคการศึกษาที่ 3/2565',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'รหัสวิชา',
                  hintText: 'กรุณากรอกรหัสวิชา',
                ),
              ),
              SizedBox(height: 10),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'กลุ่ม',
                  hintText: 'กรุณากรอกเลขกลุ่ม',
                ),
              ),
              SizedBox(height: 20),
              OutlinedButton(
                style: TextButton.styleFrom(foregroundColor: Color.fromARGB(255, 38, 112, 41)),
                onPressed: () {
                  // Respond to button press
                },
                child: Text("ค้นหา"),
              ),

              SizedBox(height: 20),
              OutlinedButton(
                style: TextButton.styleFrom(foregroundColor: Color.fromARGB(255, 255, 0, 128)),
                onPressed: () {
                  // Respond to button press
                },
                child: Text("ตรวจสอบ"),
              ),

              SizedBox(height: 20),
              OutlinedButton(
                style: TextButton.styleFrom(foregroundColor: Color.fromARGB(255, 22, 15, 119)),
                onPressed: () {
                  // Respond to button press
                },
                child: Text("ยืนยันการลงทะเบียน"),
              )
            ])));
  }
}
