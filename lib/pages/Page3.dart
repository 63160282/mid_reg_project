import 'package:flutter/material.dart';

class StudentPage3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ประวัตินิสิต'),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 16.0),
                alignment: Alignment.center,
                child: Image.network(
                  'https://scontent.fbkk22-2.fna.fbcdn.net/v/t39.30808-6/333685599_1623357164789996_8915545658782695630_n.jpg?stp=cp6_dst-jpg&_nc_cat=106&ccb=1-7&_nc_sid=8bfeb9&_nc_eui2=AeFhe6AEFhUWTjD7F_DuEKS2_yH5eShcenP_Ifl5KFx6c5h8My80tUC9TA80g9XXJPd7QLHi4I0WjPpeIIqJK6HP&_nc_ohc=qj6OY2Gj3mQAX9gfklB&_nc_oc=AQnwqcQLVtsFAfsTSHCB0yUiSgbekUSn2wKts31yvsq1-riF69CVu_L_H2YMoCVLufQ&_nc_zt=23&_nc_ht=scontent.fbkk22-2.fna&oh=00_AfAKoPdRyVV4uauc-uqXYUkSP5kfrGPuy6ZE1FxeuNPYVg&oe=6422CAD9',
                  width: 200,
                  height: 200,
                  fit: BoxFit.cover,
                ),
              ),
              Text(
                'รหัสประจำตัว: 63160282',
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8.0),
              Text(
                'เลขที่บัตรประชาชน: 1240401141990',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'ชื่อ: นางสาวจิรารัตน์ ภู่ภมร',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'ชื่ออังกฤษ: MISS JIRARAT PHOOPHAMORN',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'คณะ: คณะวิทยาการสารสนเทศ',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'วิทยาเขต: บางแสน',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'หลักสูตร: 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'วิชาโท: -',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
              Text(
                'ระดับการศึกษา: ปริญญาตรี',
                style: TextStyle(fontSize: 16.0),
              ),
              SizedBox(height: 8.0),
            ],
          ),
        ));
  }
}
