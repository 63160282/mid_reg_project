// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Emoticon extends StatelessWidget {

  final String emoticon;
  const Emoticon({Key? key,
  required this.emoticon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Color.fromARGB(255, 212, 206, 206),
          borderRadius: BorderRadius.circular(12)),
      padding: EdgeInsets.all(12),
      child: Center(
        child: Text(
          emoticon,
          style: TextStyle(
            fontSize: 30,
          ),
          ),
        
        ),
    );
  }
}
